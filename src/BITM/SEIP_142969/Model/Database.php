<?php
namespace App\Model;
//print_r(PDO::getAvailableDrivers());   //for checking whih database are installed

use PDO;
use PDOException;
class Database{
    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b35";
    public $user="root";
    public $password="";



    public function __construct()
    {

        try {

            # MySQL with PDO_MYSQL
            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user,$this->password);
            echo "Connected successfully";

        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}
